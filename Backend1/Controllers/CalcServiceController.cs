﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Backend1.Models;
using Backend1.Services;

namespace Backend1.Controllers
{
    public class CalcServiceController : Controller
    {
        private ICalcService CalcService;
        private IRandomValService RandomService;

        public CalcServiceController(ICalcService par, IRandomValService par2)
        {
            CalcService = par;
            RandomService = par2;
        }


        public IActionResult AccessServiceDirectly()
        {

            return this.View(); 
        }

        public IActionResult PassUsingModel()
        {
            var fst = RandomService.GetRandomVal(0, 10);
            var sec = RandomService.GetRandomVal(0, 10);
            var a = CalcService.CalcAdd(fst,sec);
            var s = CalcService.CalcSub(fst, sec);
            var m = CalcService.CalcMult(fst, sec);
            var d = CalcService.CalcDiv(fst, sec);
            var model = new CalcServiceViewModel
            {
                ValFst = fst,
                ValSec = sec,
                ValA = a,
                ValS = s,
                ValM = m,
                ValD = d
            };

            return this.View(model);
        }

        public IActionResult PassUsingViewBag()
        {
            var fst = RandomService.GetRandomVal(0, 10);
            var sec = RandomService.GetRandomVal(0, 10);
            var a = CalcService.CalcAdd(fst, sec);
            var s = CalcService.CalcSub(fst, sec);
            var m = CalcService.CalcMult(fst, sec);
            var d = CalcService.CalcDiv(fst, sec);

            this.ViewBag.ValFst = fst;
            this.ViewBag.ValSec = sec;
            this.ViewBag.ValA = a;
            this.ViewBag.ValS = s;
            this.ViewBag.ValM = m;
            this.ViewBag.ValD = d;

            return this.View();
        }

        public IActionResult PassUsingViewData()
        {
            var fst = RandomService.GetRandomVal(0, 10);
            var sec = RandomService.GetRandomVal(0, 10);
            var a = CalcService.CalcAdd(fst, sec);
            var s = CalcService.CalcSub(fst, sec);
            var m = CalcService.CalcMult(fst, sec);
            var d = CalcService.CalcDiv(fst, sec);

            this.ViewData["ValFst"] = fst;
            this.ViewData["ValSec"] = sec;
            this.ViewData["ValA"] = a;
            this.ViewData["ValS"] = s;
            this.ViewData["ValM"] = m;
            this.ViewData["ValD"] = d;

            return this.View();
        }
    }
}