﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend1.Models
{
    public class CalcServiceViewModel
    {
        public double ValFst { get; set; }
        public double ValSec { get; set; }

        public double ValA { get; set; }
        public double ValS { get; set; }
        public double ValM { get; set; }
        public double ValD { get; set; }
    }
}
