﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend1.Services
{
    public class RandomValService : IRandomValService
    {
        public int GetRandomVal(int fst, int scnd) {
            Random val = new Random();
            int ans = val.Next(fst,scnd);
            return ans;
        }
    }
}
