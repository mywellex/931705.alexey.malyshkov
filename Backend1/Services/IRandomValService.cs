﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend1.Services
{
    public interface IRandomValService
    {
        int GetRandomVal(int fst, int scnd);
    }
}
