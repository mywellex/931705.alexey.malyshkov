﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend1.Services
{
    public class CalcService : ICalcService
    {
       public int CalcAdd(int fst, int scnd)
        {
            return fst + scnd;
        }
        public int CalcSub(int fst, int scnd)
        {
            return fst - scnd;
        }
        public int CalcMult(int fst, int scnd)
        {
            return fst * scnd;
        }
        public double CalcDiv(int fst, int scnd)
        {
            return fst / scnd;
        }
    }
}
