﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend1.Services
{
    public interface ICalcService
    {
        int CalcAdd(int fst, int scnd);
        int CalcSub(int fst, int scnd);
        int CalcMult(int fst, int scnd);
        double CalcDiv(int fst, int scnd);
    }
}
